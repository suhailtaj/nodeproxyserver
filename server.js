// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express'); // call express
var app = express(); // define our app using express
var bodyParser = require('body-parser');
var Client = require('node-rest-client').Client;

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var port = process.env.PORT || 5000; // set our port
// var port = 5000;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); // get an instance of the express Router
var client = new Client({ user: 'customer', password: 'customer' });

// CORS enabled using the code below
app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
    next();
});

//POST CALLS

//  register customer
app.post("/customers/registerCustomerAccount", function(req, res) {
    console.log(req.body);

    var args = {
        data: req.body,
        headers: { "Content-Type": "application/json" }
    };

    // URL of the actual POST request can be changed below
    client.post('http://164.109.96.200:8080/agtech/v1/customers/registerCustomerAccount', args, function(data, response) {
        res.send(JSON.stringify(data));
    });
});

// register catalogs
app.post("/catalogs", function(req, res) {
    console.log(req.body);

    var args = {
        data: req.body,
        headers: { "Content-Type": "application/json" }
    };
    // URL of the actual POST request can be changed below
    client.post('http://164.109.96.200:8080/agtech/v1/catalogs', args, function(data, response) {
        res.send(JSON.stringify(data));
    });
});

// Create entity for particular customer
// Dynamic Values can be written using :customerId(what ever name needs to be passed in from the UI code)
app.post("/customers/:customerId/createEntity/:catalogId", function(req, res) {
    console.log(req.body);
    var args = {
        data: req.body,
        headers: { "Content-Type": "application/json" }
    };
    console.log("args.reg.body : " + args.data);
    // URL of the actual POST request can be changed below
    client.post("http://164.109.96.200:8080/agtech/v1/customers/" + req.params.customerId + "/createEntity/" + req.params.catalogId, args, function(data, response) {
        res.send(JSON.stringify(data));
        console.log("Passed data after post and printing data is " + data);
    });
});


//GET CALLS

// service call to the customers get call
app.get('/customers', function(req, res, next) {
    // URL of the actual GET request can be changed below
    client.get("http://164.109.96.200:8080/agtech/v1/customers?s=20", function(data, response) {
        console.log(data);
        dateReceived(data);
        console.log(response.statusCode);
    });

    function dateReceived(data) {
        var outerData = data;
        console.log('outerData ' + outerData);
        res.json(outerData);
    }
});

// service call to the customers get call
app.get('/catalogs', function(req, res, next) {

    // URL of the actual GET request can be changed below
    client.get("http://164.109.96.200:8080/agtech/v1/catalogs?s=20", function(data, response) {
        console.log(data);
        dateReceived(data);
    });

    function dateReceived(data) {
        var outerData = data;
        res.json(outerData);
    }
});

// service call to get all devices assigned to a particular vineyard
app.get('/customers/:customerId/entities', function(req, res, next) {
    console.log(req.params.customerId);

    // URL of the actual GET request can be changed below
    // Dynamic Values can be written using :customerId and :entityId (Make sure you add the ":" before the dynamic value
    // Dynamic value can be what ever name needs to be passed in from the UI code)
    client.get("http://164.109.96.200:8080/agtech/v1/customers/" + req.params.customerId + "/entities?s=20",
        function(data, response) {
            dataReceivedVaraibleGet(data);
        });

    function dataReceivedVaraibleGet(data) {
        var renderdata = data;
        res.json(renderdata);
        console.log("rendered data" + renderdata);
    }
});

// service call to get a specific device(entityId) assigned to a particular vineyard(customerId)
app.get('/customers/:customerId/entities/:entityId', function(req, res, next) {
    console.log(req.params.customerId);
    console.log(req.params.entityId);

    // URL of the actual GET request can be changed below
    // Dynamic Values can be written using :customerId and :entityId (Make sure you add the ":" before the dynamic value
    // Dynamic value can be what ever name needs to be passed in from the UI code)
    client.get("http://164.109.96.200:8080/agtech/v1/customers/" + req.params.customerId + "/entities/" + req.params.entityId + "?s=20",
        function(data, response) {
            getSpecificDeviceData(data);
        });

    function getSpecificDeviceData(data) {
        var renderdata = data;
        res.json(renderdata);
    }

});


//PUT CALLS

// Service to update a specific entity for a customer
// Dynamic Values can be written using :customerId and :entityId (Make sure you add the ":" before the dynamic value
// Dynamic value can be what ever name needs to be passed in from the UI code)
app.put('/customers/:customerId/entities/:entityId', function(req, res) {
    console.log(req.params.customerId);
    console.log(req.params.entityId);
    console.log(req.body);

    var args = {
        data: req.body
    };
    console.log("args.data : " + args.data);

    // URL of the actual PUT request can be changed below
    client.put("http://164.109.96.200:8080/agtech/v1/customers/" + req.params.customerId + "/entities/" + req.params.entityId,
        args,
        function(data, response) {
            getSpecificDeviceData(data);
        });

    function getSpecificDeviceData(data) {
        console.log(data);
        var renderdata = data;
        res.json({ "status": res.statusCode });
    }
});

app.use('/*', router);

app.listen(port);

// Currently accessible GET URLS:
console.log('http://localhost:' + port + '/customers');
console.log('http://localhost:' + port + '/catalogs');
